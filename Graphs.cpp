//============================================================================
// Name        : Graphs.cpp
// Created on  : 25.05.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Utilities class 
//============================================================================

#include "Graphs.h"

#include <iostream>
#include <algorithm>

#include <list>
#include <deque>
#include <map>
#include <vector>


namespace Graphs {

	template<typename T>
	std::ostream& operator<<(std::ostream& stream, const std::vector<T>& vect) {
		for (const T& entry : vect)
			stream << entry << ' ';
		return stream;
	}

	template<typename T>
	std::ostream& operator<<(std::ostream& stream, const std::list<T>& vect) {
		for (const T& entry : vect)
			stream << entry << ' ';
		return stream;
	}
}

namespace Graphs::Tests {

	class Graph {
	private:
		std::map<int, std::vector<int>> nodes;

	public:

		void addEdge(int v, int w) {
			nodes[v].push_back(w);
		}

		const inline size_t size() const noexcept {
			return this->nodes.size();
		}

		const auto& operator[](size_t index) const {
			return this->nodes.at(index);
		}
	};

	void BFS(const Graph& graphs, int s)
	{
		std::vector<bool> visited(graphs.size(), false);

		visited[s] = true;
		std::deque<int> queue{ s }; // deque contains 1-st node

		while (false == queue.empty()) {
			s = queue.front();
			queue.pop_front();
			std::cout << s << " ";

			for (const auto& nodes = graphs[s]; auto it: nodes) {
				if (false == visited[it]) {
					visited[it] = true;
					queue.push_back(it);
				}
			}
		}
	}


	void BFS_Test() {
		// Create a graph given in the above diagram 
		Graph graphs;
		graphs.addEdge(0, 1);
		graphs.addEdge(0, 2);
		graphs.addEdge(1, 2);
		graphs.addEdge(2, 0);
		graphs.addEdge(2, 3);
		graphs.addEdge(3, 3);

		std::cout << "Following is Breadth First Traversal (starting from vertex 2):" << std::endl;
		BFS(graphs, 2);
		std::cout << std::endl;
	}
}

namespace Graphs::DFS_Tests {

	class Graph
	{
	public:
		std::map<int, bool> visited;
		std::map<int, std::vector<int>> nodes;

		void addEdge(int v, int w) {
			nodes[v].push_back(w); // Add w to v�s list.
		}

		void DFS(int v) {
			// Mark the current node as visited and print it
			visited[v] = true;
			std::cout << v << " ";

			// Recur for all the vertices adjacent to this current node
			for (const auto id : nodes[v])
				if (false == visited[id])
					DFS(id);
		}
	};

	void Test()
	{
		// Create a graph given in the above diagram
		Graph g;
		g.addEdge(0, 1);
		g.addEdge(0, 9);
		g.addEdge(1, 2);
		g.addEdge(2, 0);
		g.addEdge(2, 3);
		g.addEdge(9, 3);

		std::cout << "Following is Depth First Traversal (starting from vertex 2) \n";
		g.DFS(2);
	}
}


namespace Graphs::Find_All_Paths {

	class Graph
	{
	public:
		std::map<int, bool> visited;
		std::map<int, std::vector<int>> nodes;
		std::vector<int> path;

		void addEdge(int v, int w) {
			nodes[v].push_back(w);
		}

		void FindPaths(int v, int node_to_find) {
			visited[v] = true;
			path.push_back(v);

			if (v == node_to_find) {
				// Print full path
				std::cout << path << std::endl;
			}
			else {
				for (const auto id : nodes[v])
					if (false == visited[id])
						FindPaths(id, node_to_find);
			}

			// To get the entire list of available paths, you need to mark the nodes as not viewed 
			// (after the completion of the recursion on this node - otherwise we will go even partially along this 
			// path) + delete these nodes and the path itself
			path.pop_back();
			visited[v] = false;
		}
	};

	void Test()
	{
		Graph g;
		g.addEdge(0, 1);
		g.addEdge(0, 2);
		g.addEdge(0, 3);
		g.addEdge(2, 0);
		g.addEdge(2, 1);
		g.addEdge(1, 3);


		g.FindPaths(2, 3);
	}
}

namespace Graphs::Find_Longest_Path {

	class Graph
	{
	public:
		std::map<int, bool> visited;
		std::map<int, std::vector<int>> nodes;

		std::vector<int> path;
		std::vector<int> max_path;

		void addEdge(int v, int w) {
			nodes[v].push_back(w);
		}

		void FindPaths(int v, int node_to_find) {
			visited[v] = true;
			path.push_back(v);

			if (v == node_to_find) {
				if (path.size() > max_path.size())
					max_path.assign(path.begin(), path.end());
			}
			else {
				for (const auto id : nodes[v])
					if (false == visited[id])
						FindPaths(id, node_to_find);
			}

			// To get the entire list of available paths, you need to mark the nodes as not viewed 
			// (after the completion of the recursion on this node - otherwise we will go even partially along this 
			// path) + delete these nodes and the path itself
			path.pop_back();
			visited[v] = false;
		}

		void print_max() {
			std::cout << max_path << std::endl;
		}
	};

	void Test()
	{
		Graph g;
		g.addEdge(0, 1);
		g.addEdge(0, 2);
		g.addEdge(0, 3);
		g.addEdge(2, 0);
		g.addEdge(2, 1);
		g.addEdge(1, 3);


		g.FindPaths(2, 3);
		g.print_max();
	}
}


namespace Graphs::Find_Shortest_Path {

	class Graph
	{
	public:
		std::map<int, bool> visited;
		std::map<int, std::vector<int>> nodes;

		std::vector<int> path;

		std::vector<int> longest_path;
		std::vector<int> shortest_path;

		void addEdge(int v, int w) {
			nodes[v].push_back(w);
		}

		void FindPaths(int v, int node_to_find) {
			visited[v] = true;
			path.push_back(v);

			if (v == node_to_find) {
				if (shortest_path.empty())
					shortest_path.assign(path.begin(), path.end());
				else if (shortest_path.size() > path.size())
					shortest_path.assign(path.begin(), path.end());

				if (path.size() > longest_path.size())
					longest_path.assign(path.begin(), path.end());
			}
			else {
				for (const auto id : nodes[v])
					if (false == visited[id])
						FindPaths(id, node_to_find);
			}

			// To get the entire list of available paths, you need to mark the nodes as not viewed 
			// (after the completion of the recursion on this node - otherwise we will go even partially along this 
			// path) + delete these nodes and the path itself
			path.pop_back();
			visited[v] = false;
		}

		void print_max() {
			std::cout << "longest_path = " << longest_path << std::endl;
			std::cout << "shortest_path = " << shortest_path << std::endl;
		}
	};

	void Test()
	{
		Graph g;
		g.addEdge(0, 1);
		g.addEdge(0, 2);
		g.addEdge(0, 3);
		g.addEdge(2, 0);
		g.addEdge(2, 1);
		g.addEdge(1, 3);


		g.FindPaths(2, 3);
		g.print_max();
	}
}

void Graphs::TEST_ALL()
{
	// Tests::BFS_Test();

	// DFS_Tests::Test();

	// Find_All_Paths::Test();
	// Find_Longest_Path::Test();
	 Find_Shortest_Path::Test();
};
