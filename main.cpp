//============================================================================
// Name        : Algoritms MAIN.cpp
// Created on  : August 12, 2019
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Algoritms
//============================================================================

#include <chrono>
#include <thread>

#include "Lists.h"
#include "Heap.h"
#include "Graphs.h"
#include "Trees.h"
#include "Queues.h"
#include "Stack.h"
#include "Sorting.h"
#include "Strings.h"

int main(int argc, char** argv)
{
	// Graphs::TEST_ALL();
	// LinkedList::TEST_ALL();
	// Heap_Algoritms::TEST_ALL();
	// Trees::TEST_ALL();
	// Queues::TEST_ALL();
	// Stack::TEST_ALL();
	// Sorting::TEST_ALL();
	Strings::TEST_ALL();

	std::this_thread::sleep_for(std::chrono::seconds(100));
	return 0;
}
