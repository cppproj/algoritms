//============================================================================
// Name        : Stack.h
// Created on  : 16.07.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Stack tests class 
//============================================================================

#ifndef STACK_TESTS_INCLUDE_GUARD__H
#define STACK_TESTS_INCLUDE_GUARD__H

namespace Stack {
	void TEST_ALL();
};

#endif // !STACK_TESTS_INCLUDE_GUARD__H

