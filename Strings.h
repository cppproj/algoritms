//============================================================================
// Name        : Strings.h
// Created on  : 13.07.2021
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Strings tests class 
//============================================================================

#ifndef STRINGS_TESTS_INCLUDE_GUARD__H
#define STRINGS_TESTS_INCLUDE_GUARD__H

namespace Strings {
	void TEST_ALL();
};

#endif // !STRINGS_TESTS_INCLUDE_GUARD__H

