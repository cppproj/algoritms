//============================================================================
// Name        : LinkedList.h
// Created on  : 03.05.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Linked lists list tests class 
//============================================================================

#ifndef LNKED_LIST_SIMPLE_TESTS_INCLUDE_GUARD__H
#define LNKED_LIST_SIMPLE_TESTS_INCLUDE_GUARD__H

#include <iostream>

namespace LinkedList {
	void TEST_ALL();
};

#endif // !LNKED_LIST_SIMPLE_TESTS_INCLUDE_GUARD__H
